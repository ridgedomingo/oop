import java.util.*;

public class Sample {
    public static void main(String[] args) {
        Domain elections = new Domain("Elections");

        Topic gracePoe = new Topic(elections, "Grace Poe");
        Topic jejomarBinay = new Topic(elections, "Jejomar Binay");
        Topic miriamSantiago = new Topic(elections, "Miriam Santiago");
        Topic marRoxas = new Topic(elections, "Mar Roxas");
        Topic ferdinandMarcos = new Topic(elections, "Ferdinand Marcos Jr.");

        Sentiment positive = new Sentiment("+");
        Sentiment negative = new Sentiment("-");




        Domain[] domains = new Domain[]{
                elections
        };

        Topic[] topics = new Topic[]{
                gracePoe, jejomarBinay, miriamSantiago, marRoxas, ferdinandMarcos
        };


                Sentiment[] sentiments = new Sentiment[]{
                positive, negative,
        };

        List<Document> trainingDocuments = new ArrayList<Document>();


        trainingDocuments.add(new Document("Grace Grace Poe is weak! She's a loser!!!", new TopicSentiment[]{
                new TopicSentiment(gracePoe, negative)
        }));
        trainingDocuments.add(new Document("Grace Poe is a very strong independent woman.", new TopicSentiment[]{
                new TopicSentiment(gracePoe, positive)
        }));
        trainingDocuments.add(new Document("Binay is a nognog.", new TopicSentiment[]{
                new TopicSentiment(jejomarBinay, negative)
        }));
        trainingDocuments.add(new Document("Binay is a very very very nognog.", new TopicSentiment[]{
                new TopicSentiment(jejomarBinay, negative)
        }));
        trainingDocuments.add(new Document("Kakainin ng buhay ni Miriam si Binay", new TopicSentiment[]{
                new TopicSentiment(miriamSantiago, positive),
                new TopicSentiment(jejomarBinay, negative),
        }));
        trainingDocuments.add(new Document("Go Miriam! Kaya mo yan!", new TopicSentiment[]{
                new TopicSentiment(miriamSantiago, positive),
        }));
        trainingDocuments.add(new Document("Binay is weak.", new TopicSentiment[]{
                new TopicSentiment(jejomarBinay, negative)
        }));
        trainingDocuments.add(new Document("Poe ano ka ba???", new TopicSentiment[]{
                new TopicSentiment(gracePoe, negative)
        }));
        trainingDocuments.add(new Document("Independent woman ba kamo?", new TopicSentiment[]{
                new TopicSentiment(gracePoe, negative)
        }));
        trainingDocuments.add(new Document("Nognog ka talaga!", new TopicSentiment[]{
                new TopicSentiment(jejomarBinay, negative)
        }));
        trainingDocuments.add(new Document("Binay baket ambait mo? Sobra.", new TopicSentiment[]{
                new TopicSentiment(jejomarBinay, negative)
        }));
        trainingDocuments.add(new Document("Bakit feeling ko mabait naman si Binay?", new TopicSentiment[]{
                new TopicSentiment(jejomarBinay, positive)
        }));
        trainingDocuments.add(new Document("Yari si binay kay miriam. Hahahaha!", new TopicSentiment[]{
                new TopicSentiment(miriamSantiago, positive),
                new TopicSentiment(jejomarBinay, negative),
        }));
        trainingDocuments.add(new Document("Mabait naman si binay ah", new TopicSentiment[]{
                new TopicSentiment(jejomarBinay, positive),
        }));
        trainingDocuments.add(new Document("Puro salita lang yang si Miriam uy", new TopicSentiment[]{
                new TopicSentiment(miriamSantiago, negative),
        }));
        trainingDocuments.add(new Document("Mar Roxas? Uto-uto lang yan!", new TopicSentiment[]{
                new TopicSentiment(marRoxas, negative),
        }));
        trainingDocuments.add(new Document("Kalyeserye at the moment with #AlDub", new TopicSentiment[]{
        }));
        trainingDocuments.add(new Document("Sino dito ang boboto kay Grace Poe? Wala!", new TopicSentiment[]{
                new TopicSentiment(gracePoe, negative),
        }));
        trainingDocuments.add(new Document("Mahirap magbigay ng tiwala kay Binay eh", new TopicSentiment[]{
                new TopicSentiment(jejomarBinay, negative),
        }));
        trainingDocuments.add(new Document("Si Mar Roxas ang magtutuloy sa matuwid na daan ni PNoy!", new TopicSentiment[]{
                new TopicSentiment(marRoxas, positive),
        }));
        trainingDocuments.add(new Document("Naniniwala akong gaganda ang Pilipinas tulad ng Makati " +
                "kung si Binay ang magiging presidente", new TopicSentiment[]{
                new TopicSentiment(jejomarBinay, positive),
        }));
        trainingDocuments.add(new Document("I believe that Binay will turn the Philippines " +
                "to something like Makati", new TopicSentiment[]{
                new TopicSentiment(jejomarBinay, positive),
        }));
        trainingDocuments.add(new Document("Grabe, ang ganda ng commercial ni Grace ha.", new TopicSentiment[]{
                new TopicSentiment(gracePoe, positive),
        }));
        trainingDocuments.add(new Document("OMG Basahin mo to! @somegurl!", new TopicSentiment[]{
        }));
        trainingDocuments.add(new Document("Ang ganda talaga ng Aldub sa may Philippine Arena", new TopicSentiment[]{
        }));
        trainingDocuments.add(new Document("Bongbong Marcos is a pretty good liar!", new TopicSentiment[]{
                new TopicSentiment(ferdinandMarcos, negative),
        }));
        trainingDocuments.add(new Document("Miriam made a big mistake getting Marcos as her VP!", new TopicSentiment[]{
                new TopicSentiment(miriamSantiago, negative),
                new TopicSentiment(ferdinandMarcos, negative),
        }));
        trainingDocuments.add(new Document("Ferdinand Marcos Jr. does have a point.", new TopicSentiment[]{
                new TopicSentiment(ferdinandMarcos, positive),
        }));


        Map<String, Integer> wordCount = new HashMap<>();
        Map<String, Integer> pairCount = new HashMap<>();
        Map<Integer, Integer> letterCount = new HashMap<>();
        int binaycount=0;
        int miriamcount=0;
        int gracecount=0;
        int roxascount=0;
        int marcoscount=0;
        String b = "binay";
        String s = "miriam";
        String p = "poe";
        String r = "roxas";
        String m = "marcos";

        for(Document value: trainingDocuments) {

            //  System.out.println(value.docs);
            String docu = value.docs.toLowerCase();
            String[] string2 = docu.split(" ");
            String[] get = docu.trim().split("\\n");
            StringTokenizer token = new StringTokenizer(docu);
           // System.out.print(docu);

            
            for (String g : get) {
                //NUMBER OF DOCU PER TOPIC
                if (g.contains(b)) {
                    binaycount = binaycount + 1;

                } else if (g.contains(s)) {
                    miriamcount = miriamcount + 1;

                }
                else if (g.contains(p)){
                    gracecount = gracecount +1;
                }
                else if (g.contains(r)) {
                    roxascount = roxascount + 1;

                }
                else if (g.contains(m)){
                    marcoscount = marcoscount +1;
                }

                System.out.print("Number of docu BINAY = " + binaycount + "\n");
                System.out.print("Number of docu MIRIAM = " + miriamcount + "\n");
                System.out.print("Number of docu POE = " + gracecount + "\n");
                System.out.print("Number of docu ROXAS = " + roxascount + "\n");
                System.out.print("Number of docu MARCOS = " + marcoscount + "\n");

            }
            //TOPICWITHMOSTNUMBEROFDOCU
            if (binaycount > miriamcount && binaycount > gracecount && binaycount > roxascount && binaycount > marcoscount) {
                System.out.print("DOCU WITH MOST NUMBER OF TOPIC = Jejomar Binay"+"\n");


            } else if (miriamcount > binaycount && miriamcount > gracecount && miriamcount > roxascount && miriamcount > marcoscount) {
               System.out.print("DOCU WITH MOST NUMBER OF TOPIC = Miriam Defensor Santiago"+"\n");
            } else if (gracecount > binaycount && gracecount > miriamcount && gracecount > roxascount && gracecount > marcoscount) {
                System.out.print("DOCU WITH MOST NUMBER OF TOPIC = Grace Poe"+"\n");
            } else if (roxascount > binaycount && roxascount > miriamcount && roxascount > gracecount && roxascount > marcoscount) {
                System.out.print("DOCU WITH MOST NUMBER OF TOPIC = Mar Roxas"+"\n");
            } else if (marcoscount > binaycount && marcoscount > miriamcount && marcoscount > gracecount && marcoscount > roxascount) {
                System.out.print("DOCU WITH MOST NUMBER OF TOPIC = Bongbong Marcos"+"\n");
            }
            //WORDCOUNT

            while (token.hasMoreTokens()) {
                String word = token.nextToken().toLowerCase();

                Integer count = wordCount.get(word);

                if (count == null) {
                    wordCount.put(word, 1);

                } else {
                    wordCount.put(word, count + 1);
                }
            }
            for (String tokenize : wordCount.keySet()) {
                System.out.println("Word : " + tokenize + " has count :" + wordCount.get(tokenize));


            }

            //PAIRCOUNT

            for (int i = 0; i < string2.length - 1; i++) {
                String temp = string2[i] + " " + string2[i + 1];
                temp = temp.toLowerCase();
                if (docu.toLowerCase().contains(temp)) {
                    if (pairCount.containsKey(temp))
                        pairCount.put(temp, pairCount.get(temp) + 1);
                    else
                        pairCount.put(temp, 1);
                }

            }
            System.out.println(pairCount);

            //LETTERCOUNT

            for (int i = 0; i < docu.length(); i++) {
                char c = docu.charAt(i);

                if (c != ' ') {

                    int v = letterCount.getOrDefault((int) c, 0);
                    letterCount.put((int) c, v + 1);
                }
            }

            for (int key : letterCount.keySet()) {
                System.out.println((char) key + ": " + letterCount.get(key));
            }


        }




    }

}
